<?php
    
    require_once '../classes/SQL.php';

    $Connect = mysqli_connect('localhost', 'raf', 'admin123', 'baza1');
    
    if (isset($_POST['delete']) && $_POST['delete'] != NULL) {
        $Query = "DELETE FROM users WHERE user_id = '{$_POST['delete']}'";
        
        mysqli_query($Connect, $Query);
    }
    
    if ($_POST['user_name'] != NULL) {
        
        $userName = $_POST['user_name'];
        $userEmail = $_POST['user_email'];
        $userPassword = $_POST['user_password'];
        
        if (mysqli_query($Connect, "SELECT * FROM users WHERE user_name = '{$userName}'")->num_rows != 0) {
            echo 'Użytkownik już istnieje.';
        }
        else {
            $Query = "INSERT INTO users (user_id, user_name, user_email, user_password, last_log_in) "
                    . "VALUES ('', '{$userName}', '{$userEmail}', '{$userPassword}', NOW())";
            mysqli_query($Connect, $Query);
        }
        echo $Query;
    }

?>
<link href="style.css" rel="stylesheet" type="text/css"/>

<div class="AddItem login-page">
    <div class="form" style="margin: 0 auto">
    <form method="POST">
        
        <div class="row">
            <label>Użytkownik</label>
            <input type="text" name="user_name" value="" />
        </div>
        
        <div class="row">
            <label>Email</label>
            <input type="text" name="user_email" value="" />
        </div>
        
        <div class="row">
            <label>Hasło</label>
            <input type="text" name="user_password" value="" />
        </div>
        
        <div class="submit">
            <input type="submit" value="dodaj" />
        </div>
        
    </form>
    </div>
</div>

<?php

$Table = $sql->select('users');

if ($Table) {
    
    ?>

<table>
    
    <tr>
        <th>L.P.</th>
        <th>Użytkownik</th>
        <th>Email</th>
        <th>Hasło</th>
        <th>Data logowania</th>
        <th>Usuń</th>
    </tr>
    
    <?php
    $nr = 1;
    foreach ($Table as $single) {
        
        echo '<tr>';
            echo '<td>' . $nr++ . '</td>';
            echo '<td>' . $single['user_name'] . '</td>';
            echo '<td>' . $single['user_email'] . '</td>';
            echo '<td>' . $single['user_password'] . '</td>';
            echo '<td>' . $single['last_log_in'] . '</td>';
            echo '<td><form method="POST"><input type="submit" value="Usuń">'
            . '<input type="hidden" name="delete" value="' . $single['user_id'] . '" /></form></td>';
        echo '</tr>';
    }
    ?>
    
</table>

<?php
    
}